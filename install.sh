#!/bin/bash

function pre_install_update() {
    echo "=== pre_install_update ===";
    sudo apt update;
    sudo apt upgrade -y;
}

function install_tools() {
    echo "=== install_tools ==="
    sudo apt install -y \
        chrome-gnome-shell \
        git \
        git-email \
        git-gui \
        gitk \
        kdiff3 \
        mc \
        meld \
        nfs-kernel-server \
        vim
    sudo apt install -y bash-completion clangd
    sudo apt install -y default-jre wget curl
}

function install_code() {
    echo "=== install_code ==="
    sudo snap install --classic code
}

function install_chrome() {
    echo "=== install_chrome ==="
    sudo snap install --classic chromium
}

function git_clone_linux() {
    echo "=== git_clone_linux ==="
    mkdir -p ~/linux-kernel-labs/src
    pushd ~/linux-kernel-labs/src
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    popd
}

function extract_lab() {
    echo "=== extract_lab ==="
    local doc=${HOME}/linux-kernel-labs/doc/
    mkdir -p ${doc}/res
    wget https://bootlin.com/doc/training/linux-kernel/linux-kernel-slides.pdf
    wget https://bootlin.com/doc/training/linux-kernel/linux-kernel-labs.pdf
    wget https://bootlin.com/doc/training/linux-kernel/linux-kernel-labs.tar.xz
    tar xf linux-kernel-labs.tar.xz -C ${HOME}/
    mv *.pdf ${doc}
}

function do_docker() {
    echo "=== do_docker ==="
    sudo snap install --classic docker
    sudo addgroup --system docker
    sudo adduser $USER docker
    echo '{ "log-level": "error", "storage-driver": "vfs" }' | sudo tee /var/snap/docker/current/config/daemon.json
    # local lab=${HOME}/linux-kernel-labs
    # mkdir -p ${lab}/docker
    # cp env.sh ${lab}
    # cp docker/* ${lab}/docker
    # you may need manually clear storage cache:
    # docker system prune -v
    # sudo snap restart docker
}

function install_all() {
    echo "=== start ==="
    pre_install_update
    install_tools
    install_code
    install_chrome
    extract_lab
    git_clone_linux
    do_docker
    echo "=== done ==="
}

function main() {
    [[ "" == "$1" ]]        && install_all;
    [[ "zsh" == "$1" ]]     && install_zsh;
    [[ "code" == "$1" ]]    && install_code;
    [[ "lab" == "$1" ]]     && extract_lab;
    [[ "kernel" == "$1" ]]  && git_clone_linux;
    [[ "chrome" == "$1" ]]  && install_chrome;
    [[ "tools" == "$1" ]]   && install_tools;
    [[ "docker" == "$1" ]]  && do_docker;
}

main $@

