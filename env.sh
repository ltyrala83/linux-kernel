#!/bin/bash

image_name=linux_gcc_arm

function build() {
    DOCKER_BUILDKIT=1 docker build -t ${image_name} docker
}

function run() {
    docker image inspect ${image_name} > /dev/null
    [[ $? -eq 0 ]] || build

    docker run -it \
    --rm \
    --group-add $(id -g) \
    --user $(id -u):$(id -g) \
    --volume /etc/group:/etc/group \
    --volume /etc/passwd:/etc/passwd \
    --volume ${HOME}/.gitconfig:${HOME}/.gitconfig \
    --volume=${HOME}/.bashrc:${HOME}/.bashrc \
    --volume=${HOME}/.bash_history_env:${HOME}/.bash_history \
    --volume $(pwd):$(pwd) \
    --workdir $(pwd) \
    --mount type=volume,source=ccache0,target=${HOME}/.ccache \
    ${image_name} /bin/bash --rcfile /opt/bashrc
}

[[ "build" == "$1" ]] && {
    build;
} || {
    run;
}

