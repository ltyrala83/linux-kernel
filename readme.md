[TOC]

# About

This is an introduction material to [Embedded Linux kernel and driver development training](https://bootlin.com/training/kernel/)
The training is prepared and maintained by [Bootlin company](https://bootlin.com).

The course is published on open source license [Attribution-ShareAlike 3.0](http://creativecommons.org/licenses/by-sa/3.0/).
This means that we are free to:
> Share — copy and redistribute the material in any medium or format.  
> Adapt — remix, transform, and build upon the material for any purpose, even commercially.

# Agenda

The full agenda is available with training materials: [on-site training](https://bootlin.com/doc/training/linux-kernel/linux-kernel-agenda.pdf).

Brief summary:
- Kernel configuration, native and cross-compilation.
- Linux device drivers, the device model.
- Kernel frameworks, character devices.
- Memory management.
- Processes, scheduling, sleeping, and interrupts, locking.
- Driver debugging techniques.

# HW requirements

 - 1x [beagle bone black](https://pl.farnell.com/seeed-studio/102110420/beaglebone-black/dp/3520081?st=beaglebone)
 - 2x [usb serial cable](https://www.olimex.com/Products/Components/Cables/USB-Serial-Cable/USB-SERIAL-F/)
 - 1x [joystick + uext connector](https://www.olimex.com/Products/Modules/Sensors/MOD-WII/MOD-Wii-UEXT-NUNCHUCK/)
 - 1x [wire set](https://www.olimex.com/Products/Breadboarding/JUMPER-WIRES/JW-110x10/)
 - 1x 4-Port USB Hub: [something like this](https://www.amazon.com/AmazonBasics-Port-2-5A-power-adapter/dp/B00DQFGH80)

# HW setup

The lab requires erasing MMC on the Beagle Bone board.
The board shall have only u-boot bootloader installed.
The user shall flash a micro-SD card and use it with the board.

[Detailed procedure on how to prepare the board.](https://github.com/bootlin/training-materials/tree/master/lab-data/common/bootloader/beaglebone-black)

# PC/OS requirements

```
git clone https://gitlab.com/ltyrala83/linux-kernel.git ${HOME}/linux-kernel-labs
```

The training is prepared for `Ubuntu` on a PC host, tested with `LTS 22.04` version.
The minimum configuration can be done with the attached `res/install.sh` script.
If you didn't have docker installed, you shall reboot your PC.

```shell
cd res
./install.sh

# lab dir
tree -L 3 ${HOME}/linux-kernel-labs
linux-kernel-labs
├── doc
│   ├── linux-kernel-labs.pdf
│   ├── linux-kernel-slides.pdf
│   └── res
├── docker
│   └── Dockerfile
├── env.sh
├── modules
│   ├── data
│   └── nfsroot
└── src
    └── linux
```

# Docker Image

The kernel is built under the docker container.
Test docker environment.

```shell
cd ${HOME}/linux-kernel-labs
./env.sh
# docker container shall be built and then attached
(arm) ✔ ~/linux-kernel-labs
$ alias
alias make='ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- make'
```

# Git clone/copy files

The git repository and files required by the lab shall be downloaded by the `env/install.sh` script.

# IDE for C

I suggest using the latest vs code with pre-installed [clangd extension](https://marketplace.visualstudio.com/items?itemName=llvm-vs-code-extensions.vscode-clangd).
When you succeed with the Linux kernel build, you shall generate `compile_commands.json` file.
```shell
cd ${HOME}/linux-kernel-labs
./env.sh
# linux kernel build
cd src/linux
# ...
make

# compilation database
python3 scripts/clang-tools/gen_compile_commands.py

# Open vs code in `src/linux` root directory
code .
```

# Wiring

We use several interfaces to connect to the Beagle Bone board:
- `serial cable` for:
    - **u-boot**
    - **Linux terminal**
- `usb cable` for:
    - **board power**
    - **network gadget** - network over USB
    - **nfs mount** - The Board's root file system mounted on PC
    - **ssh** optionally

```plantuml
component PC {
    component eth_pc
    portout usb_pc
    component nfs
    portout serial_pc
}
component BeagleBone {
    portin usb_bb
    component eth_bb
    component [/]
    portin serial_bb
}

eth_pc .. usb_pc
usb_pc --> usb_bb
usb_bb .. eth_bb

[/] .right[#darkgreen].-> eth_bb
eth_pc .left[#darkgreen].-> nfs
serial_pc --> serial_bb
```

# PC to BeagleBone's tty connection

The last step is to verify tty connection.
See `linux-kernel-labs/doc/linux-kernel-labs.pdf` document, `Board setup` exercise.

```shell
picocom -b 115200 /dev/ttyUSB0

U-Boot SPL 2019.04-00002-g07d5700e21 (Mar 06 2020 - 11:24:55 -0600)
Trying to boot from MMC2
Loading Environment from EXT4... 
** Unable to use mmc 0:1 for loading the env **


U-Boot 2019.04-00002-g07d5700e21 (Mar 06 2020 - 11:24:55 -0600), Build: jenkins-github_Bootloader-Builder-137

CPU  : AM335X-GP rev 2.1
I2C:   ready
DRAM:  512 MiB
No match for driver 'omap_hsmmc'
No match for driver 'omap_hsmmc'
Some drivers were not found
Reset Source: Global external warm reset has occurred.
Reset Source: Global warm SW reset has occurred.
Reset Source: Power-on reset has occurred.
RTC 32KCLK Source: External.
MMC:   OMAP SD/MMC: 0, OMAP SD/MMC: 1
Loading Environment from EXT4... 
** Unable to use mmc 0:1 for loading the env **
Board: BeagleBone Black
<ethaddr> not set. Validating first E-fuse MAC
BeagleBone Black:
Model: SeeedStudio BeagleBone Green:
BeagleBone: cape eeprom: i2c_probe: 0x54:
BeagleBone: cape eeprom: i2c_probe: 0x55:
BeagleBone: cape eeprom: i2c_probe: 0x56:
BeagleBone: cape eeprom: i2c_probe: 0x57:
Net:   eth0: MII MODE
cpsw, usb_ether
Press SPACE to abort autoboot in 0 seconds
=>
```

![bbgreen](res/doc/bbgreen.jpg)
